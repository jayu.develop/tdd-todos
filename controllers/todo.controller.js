const TodoModel = require("../model/todo.model");

exports.createTodo = async (req, res, next) => {
  try {
    const createModel = await TodoModel.create(req.body);
    res.status(201).json(createModel);
  } catch (err) {
    next(err);
  }
};

exports.getTodos = async (req, res, next) => {
  try {
    const allTodos = await TodoModel.find({});
    res.status(200).json(allTodos);
  } catch (err) {
    next(err);
  }
};

exports.getTodoById = async (req, res, next) => {
  try {
    const getTodosById = await TodoModel.findById(req.params.todoId);
    if (getTodosById) {
      res.status(200).json(getTodosById);
    } else {
      res.status(404).json({ err: true, msg: "Data not found." });
    }
  } catch (err) {
    next(err);
  }
};

exports.updateTodo = async (req, res, next) => {
  try {
    const updatedTodo = await TodoModel.findByIdAndUpdate(req.params.todoId, req.body, {
      new: true,
      useFindAndModify: false
    });
    if (updatedTodo) {
      res.status(200).json(updatedTodo);
    } else {
      res.status(404).json({ err: true, msg: "Data not found." });
    }
  } catch (err) {
    next(err);
  }
};

exports.deleteTodo = async (req, res, next) => {
  try {
    const deletedTodo = await TodoModel.findByIdAndDelete(req.params.todoId);
    if (deletedTodo) {
      res.status(200).json(deletedTodo);
    } else {
      res.status(404).json({ err: true, msg: "Data not found." })
    }
  } catch (err) {
    next(err);
  }
};
